/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digikala;

import java.util.List;

/**
 *
 * @author vahid
 */
public class STDDevCalculator {

    public static void method1(List<Double> list) throws Exception {
        double d = 0, v = 0;
        int i = 0;
        for (Double line : list) {
            d += line;
        }
        double avg = d / list.size();

        for (Double line : list) {
            v += Math.pow(line - avg, 2);
        }
        double var = v / list.size();
        double en = Math.sqrt(var);

        System.out.println("+++++++Method1+++++++++++");
        System.out.println("Mean : " + avg);
        System.out.println("STDDev: " + en);
        System.out.println("+++++++++++++++++++++++++");
    }

    public static void method2(List<Double> list) throws Exception {

        double r1 = 0, r2 = 0;
        for (Double line : list) {
            r1 += line;
            r2 += line * line;
        }
        double mean = r1 / list.size();
        double stddev = Math.sqrt(r2 / list.size() - (mean * mean));
        System.out.println("\n+++++++Method2+++++++++++");
        System.out.println("Mean : " + mean);
        System.out.println("STDDev : " + stddev);
        System.out.println("+++++++++++++++++++++++++");

    }

}
