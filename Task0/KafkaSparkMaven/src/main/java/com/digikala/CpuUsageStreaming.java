/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.digikala;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function3;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.State;
import org.apache.spark.streaming.StateSpec;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaMapWithStateDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import scala.Tuple2;

/**
 *
 * @author vahid
 */
public class CpuUsageStreaming {

    final static Logger logger = Logger.getLogger(CpuUsageStreaming.class);
    public static void main(String[] args) throws Exception {
        
        Logger.getLogger("org").setLevel(Level.WARN);
        SparkConf sparkConf = new SparkConf().setAppName("CPU_USAGE_STREAM");
        JavaStreamingContext ssc = new JavaStreamingContext(sparkConf, Durations.seconds(4));
        ssc.checkpoint("/chkpointdir/");

        @SuppressWarnings("unchecked")
        List<Tuple2<String, Map<String, Double>>> tuples
                = Arrays.asList(new Tuple2<>("key", new HashMap<String, Double>()));
        JavaPairRDD<String, Map<String, Double>> initialState = ssc.sparkContext().parallelizePairs(tuples);

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", args[0]);
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "g1");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);

        Collection<String> topics = Arrays.asList(args[1]);
        JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(
                ssc, LocationStrategies.PreferConsistent(),
                ConsumerStrategies.<String, String>Subscribe(topics, kafkaParams));

        JavaPairDStream<String, Double> pairDStream = stream.mapToPair(new PairFunction<ConsumerRecord<String, String>, String, Double>() {
            @Override
            public Tuple2<String, Double> call(ConsumerRecord<String, String> t) throws Exception {
                return new Tuple2<>("key1", new Double(t.value()));
            }
        });

        JavaPairDStream<String, Iterable<Double>> group_pairDStream = pairDStream.groupByKey();
        Function3<String, Optional<Iterable<Double>>, State<Map<String, Double>>, Tuple2<String, String>> mappingFunc = (
                word, usage, state) -> {
            Map<String, Double> details = state.exists() ? state.get() : new HashMap<>();
            StringBuilder stringBuilder = new StringBuilder();
            String formattedDate = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date().getTime());
            stringBuilder.append(formattedDate).append(" -- ");
            double r1 = 0, r2 = 0, i = 0;
            for (Double u : usage.get()) {
                i++;
                r1 += u;
                r2 += u * u;
            }
            double local_avg = r1 / i;
            stringBuilder.append("LOC_AVG:").append(local_avg);
            if (details.containsKey("i")) {
                details.put("i", details.get("i") + i);
                details.put("r1", details.get("r1") + r1);
                details.put("r2", details.get("r2") + r2);
            } else {
                details.put("i", i);
                details.put("r1", r1);
                details.put("r2", r2);
            }

            double global_avg = details.get("r1") / details.get("i");
            double global_stddev = Math.sqrt(details.get("r2") / details.get("i") - (global_avg * global_avg));

            stringBuilder.append(", GLOBAL_AVG:").append(global_avg);
            stringBuilder.append(", STDDEV:").append(global_stddev);
            String message = "OK";
            if (Math.abs(local_avg - global_avg) >= Math.abs(2 * global_stddev)) {
                message = "ERROR";
                stringBuilder.append(",ERROR");
            } else if ((Math.abs(local_avg - global_avg) >= Math.abs(global_stddev))
                    && (Math.abs(local_avg - global_avg) < Math.abs(2 * global_stddev))) {
                message = "WARN";
                stringBuilder.append(",WARN");
            }
            state.update(details);
            logger.info(stringBuilder.toString());
            return new Tuple2<>(formattedDate, message);
        };

        JavaMapWithStateDStream<String, Iterable<Double>, Map<String, Double>, Tuple2<String, String>> streamWithState = group_pairDStream
                .mapWithState(StateSpec.function(mappingFunc).initialState(initialState));

        streamWithState.print();
        streamWithState.dstream().saveAsTextFiles("/out/result", "status");
        ssc.start();
        ssc.awaitTermination();

    }
}
