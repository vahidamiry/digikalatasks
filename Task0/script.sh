#!/bin/bash
while IFS='' read -r line || [[ -n "$line" ]]; do
if  [[ $line != \"time_millis* ]] ;
then
    echo $line | kafka-console-producer.sh --broker-list $2 --topic test --property "parse.key=true" --property "key.separator=,"
fi
done < "$1"
